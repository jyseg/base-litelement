import { LitElement, html } from 'lit-element';

class TestApi2 extends LitElement {

    static get properties() {
        return {			
            actors: {type: Array}
        };
    }

    constructor() {
        super();		
        this.actors = [];
        this.getPeopleData();	
	}
    
    render() {
        return html`
            <div></div>
        `;
    }

    getPeopleData () {
        console.log("getPeopleData");
        console.log("obteniendo actores");

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if (xhr.status === 200) {
                //console.log("petición correcta");
                
                let APIResponse = JSON.parse(xhr.responseText);
                this.actors = APIResponse.results;
                console.log("Se han obtenido " + this.actors.length + " personas.");
                
                this.dispatchEvent(
                    new CustomEvent("get-gente", {
                            detail: {
                                actores: this.actors
                            }
                        }
                        
                    )
                );
                //console.log(this.movies);



            } else {
                console.log("otro error" + xhr.status );
            }
        };

        xhr.open("GET", "https://swapi.dev/api/people/");
        xhr.send();
        //console.log("fin de getMovieData");

    }
}

customElements.define('test-api2', TestApi2)