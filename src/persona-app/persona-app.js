import { LitElement, html} from 'lit-element';
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';
import '../persona-sidebar/persona-sidebar.js';
import '../persona-stats/persona-stats.js';

class PersonaApp extends LitElement{

    static get properties() {
      return {			
        people: {type: Array}
      };
	  }	
    render() {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" 
	      integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" 
	      crossorigin="anonymous" />
        <persona-header></persona-header>
			  <div class="row">
            <persona-sidebar class="col-2" 
            @new-person="${this.newPerson}" 
            @updated-max-years-filter="${this.newMaxYearsInCompanyFilter}"></persona-sidebar>
            <persona-main  @updated-people="${this.updatedPeople}" class="col-10"></persona-main>
			  </div>
        <persona-footer></persona-footer>
        <persona-stats @updated-people-stats="${this.updatedPeopleStats}"></persona-stats>

        `;
      }

      newPerson(e) {
        //console.log("newPerson en PersonaApp");	
        this.shadowRoot.querySelector("persona-main").showPersonForm = true;
      }

      newMaxYearsInCompanyFilter(e) {
        //console.log("nuevo filtro " + e.detail.maxYearsInCompany);	
        //console.log(e.detail.filterPeople)
        this.shadowRoot.querySelector("persona-main").maxYearsInCompanyFilter = e.detail.maxYearsInCompany;
      }

      updated(changedProperties) {
        //console.log ("update en persona app");
    
        if (changedProperties.has("people")) {
    
          //console.log("ha cambiado el array people en persona app");
          this.shadowRoot.querySelector("persona-stats").people = this.people;

          
        }
    
      }

      updatedPeopleStats(e){

        //console.log("updatedpieoplestats");
        this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;
        this.shadowRoot.querySelector("persona-main").maxYearsInCompanyFilter = e.detail.peopleStats.maxYearsInCompany;
        
      }

      updatedPeople(e){

        //console.log("updatedpeople main")
        //console.log(e.detail.people)
        this.people = e.detail.people;

      }
}

customElements.define('persona-app', PersonaApp)