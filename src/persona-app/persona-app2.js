import { LitElement, html} from 'lit-element';
import '../persona-header/persona-header.js';
import '../persona-main/persona-main2.js';
import '../persona-footer/persona-footer.js';
import '../persona-sidebar/persona-sidebar.js';

class PersonaApp2 extends LitElement{
    render() {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" 
	      integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" 
	      crossorigin="anonymous" />
        <persona-header></persona-header>
			  <div class="row">
            <persona-sidebar class="col-2" @new-person="${this.newPerson}"></persona-sidebar>
            <persona-main2 class="col-10"></persona-main2>
			  </div>
        <persona-footer></persona-footer>

        `;
      }

      newPerson(e) {
        console.log("newPerson en PersonaApp2");	
        this.shadowRoot.querySelector("persona-main2").showPersonForm = true;
      }
}

customElements.define('persona-app2', PersonaApp2)