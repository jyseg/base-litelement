import { LitElement, html } from 'lit-element'; 
import '../persona-ficha-listado/persona-ficha-listado2.js'; 
import '../persona-form/persona-form2.js';  
import '../test-api/test-api2.js';
class PersonaMain2 extends LitElement {
	static get properties() {
		return {			
			people: {type: Array},
			showPersonForm: {type: Boolean}
		};
	}			

	constructor() {
		super();
			
		this.people = [];
		this.showPersonForm = false;
	}
		
	render() {
	return html`
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" 
	integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" 
	crossorigin="anonymous" />				
		<h2 class="text-center">Personas</h2>
		<div class="row" id="getListado">
			<test-api2 @get-gente="${this.actualizaActores}"></test-api2>
		</div>
		<div class="row" id="peopleList">			
			<div class="row row-cols-1 row-cols-sm-4">
				${this.people.map(
					person => 
					html`<persona-ficha-listado2 
							name="${person.name}" 
							mass="${person.mass}" 
							gender="${person.gender}" 
							@delete-person="${this.deletePerson}">
						</persona-ficha-listado2>`
				)}
			</div>
		</div>
		<div class="row">
			<persona-form2 id="personForm" class="d-none border rounded border-primary"
				@persona-form-close="${this.personFormClose}"
				@persona-form-store="${this.personFormStore}" >
			</persona-form2>
		</div>
		
	`;
	}

	updated(changedProperties) {
	//console.log("updated");	
	if (changedProperties.has("showPersonForm")) {
		//console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");
		if (this.showPersonForm === true) {
			this.showPersonFormData();
		} else {
			this.showPersonList();
		}
	}
	}

	deletePerson(e) {
	console.log("deletePerson en persona-main");
	console.log("Se va a borrar la persona de nombre " + e.detail.name);

	this.people = this.people.filter(
		person => person.name != e.detail.name
	);
	}

	showPersonList() {
	console.log("showPersonList");
	console.log("Mostrando listado de personas");


	this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
	this.shadowRoot.getElementById("personForm").classList.add("d-none");	  
	}

	showPersonFormData() {
	console.log("showPersonFormData");
	console.log("Mostrando formulario de persona");
	this.shadowRoot.getElementById("personForm").classList.remove("d-none");	  
	this.shadowRoot.getElementById("peopleList").classList.add("d-none");	 	  
	}

	personFormClose() {
	console.log("personFormClose");
	console.log("Se ha cerrado el formulario de la persona");

	this.showPersonForm = false;	
	}

	personFormStore(e) {
	console.log("personFormStore");
	console.log("Se va a almacenar una persona");	
						
	this.people.push(e.detail.person);

	console.log("Persona almacenada");	
	this.showPersonForm = false;
	}

	actualizaActores(e) {
		console.log("actualizaActores");
		
			
		for (var indexPeople = 0; indexPeople < e.detail.actores.length; indexPeople++) {
	
			this.people[indexPeople] = {}
			this.people[indexPeople].name = e.detail.actores[indexPeople].name;
			this.people[indexPeople].mass = e.detail.actores[indexPeople].mass;
			this.people[indexPeople].gender = e.detail.actores[indexPeople].gender;
			//console.log(persona);

			
						
		};

		//console.log(this.people);
		

		this.showPersonForm = false;
		this.requestUpdate();

		  
		}
	
}
customElements.define('persona-main2', PersonaMain2)