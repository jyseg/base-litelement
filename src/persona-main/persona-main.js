import { LitElement, html } from 'lit-element'; 
import '../persona-ficha-listado/persona-ficha-listado.js'; 
import '../persona-form/persona-form.js';  
import '../persona-main-dm/persona-main-dm.js';
class PersonaMain extends LitElement {
	static get properties() {
		return {			
			people: {type: Array},
			showPersonForm: {type: Boolean},
			filterPeople: {type: Object},
			maxYearsInCompanyFilter: {type: Number}
		};
	}			

	constructor() {
		super();

		this.filterPeople = {};
		this.maxYearsInCompanyFilter = 0;
	
		this.people = [];
		this.showPersonForm = false;
	}
		
	render() {
	return html`
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" 
	integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" 
	crossorigin="anonymous">				
		<h2 class="text-center">Personas</h2>
		
		<div class="row" id="peopleList">			
			<div class="row row-cols-1 row-cols-sm-4">
				${this.people.filter(
					person => person.yearsInCompany <= this.maxYearsInCompanyFilter
				).map(
					person => 
					html`<persona-ficha-listado 
							name="${person.name}" 
							yearsInCompany="${person.yearsInCompany}" 
							profile="${person.profile}" 
							.photo="${person.photo}"
							@delete-person="${this.deletePerson}"
							@info-person="${this.infoPerson}">
						</persona-ficha-listado>`
				)}
			</div>
		</div>
		<div class="row">
			<persona-form id="personForm" class="d-none border rounded border-primary"
				@persona-form-close="${this.personFormClose}"
				@persona-form-store="${this.personFormStore}" >
			</persona-form>
		</div>
		<persona-main-dm @getPeopleData="${this.getPeopleData}" ></persona-main-dm>
	`;
	}

	updated(changedProperties) {
	//console.log("updated");	
	if (changedProperties.has("showPersonForm")) {
		//console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");
		if (this.showPersonForm === true) {
			this.showPersonFormData();
		} else {
			this.showPersonList();
		}
	}

	if (changedProperties.has("people")) {
		//console.log("Ha cambiado el valor de la propiedad people en persona-main");
		
		this.dispatchEvent(new CustomEvent("updated-people", {
			detail : {
				people : this.people
			}
		}
		)); 

	}
	
	if (changedProperties.has("filterPeople")) {

		//console.log("filterPeople en persona-main");
		//console.log("Se va a filtrar por " + this.filterPeople.yearsInCompany + " años en la empresa.");
		
		
		/*
		this.people = this.people.filter(
			person => person.yearsInCompany <= this.filterPeople.yearsInCompany
		);*/

	}

	if (changedProperties.has("maxYearsInCompanyFilter")) {
		//console.log("ha cambiado maxYearsInCompanyFilter a " + this.maxYearsInCompanyFilter)
	}

	}

	deletePerson(e) {
	//console.log("deletePerson en persona-main");
	//console.log("Se va a borrar la persona de nombre " + e.detail.name);

	this.people = this.people.filter(
		person => person.name != e.detail.name
	);
	}

	
	infoPerson(e) {
		//console.log("infoPerson en persona-main");
		//console.log("Se va a mostrar información de la persona de nombre " + e.detail.name);
	
		let choosenPerson =  this.people.filter(
			person => person.name === e.detail.name
		);
		//console.log(choosenPerson);
		this.shadowRoot.getElementById("personForm").person = choosenPerson[0];
		this.shadowRoot.getElementById("personForm").editingPerson = true;
		this.showPersonForm = true;

		}

	showPersonList() {
	//console.log("showPersonList");
	//console.log("Mostrando listado de personas");
	this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
	this.shadowRoot.getElementById("personForm").classList.add("d-none");	  
	}

	showPersonFormData() {
	//console.log("showPersonFormData");
	//console.log("Mostrando formulario de persona");
	this.shadowRoot.getElementById("personForm").classList.remove("d-none");	  
	this.shadowRoot.getElementById("peopleList").classList.add("d-none");	 	  
	}

	personFormClose() {
	//console.log("personFormClose");
	//console.log("Se ha cerrado el formulario de la persona");

	this.showPersonForm = false;	
	}

	personFormStore(e) {
	//console.log("personFormStore");
	//console.log("Se va a almacenar una persona");	

	//console.log("guardar en main " + e.detail.editingPerson);
	
	if (e.detail.editingPerson) {

		//console.log("Persona actualizada " + e.detail.person.name);

		/*let indexOfPerson = this.people.findIndex(
			person => person.name === e.detail.person.name
		);

		if (indexOfPerson >= 0) {
			//console.log ("persona encontrada");
			this.people[indexOfPerson] = e.detail.person;
		};*/
		this.people = this.people.map(

			person => person.name === e.detail.person.name
				? person = e.detail.person : person

		)

	} else {

		//console.log("Persona almacenada nueva");
		this.people = [...this.people, e.detail.person];
		//console.log("Persona almacenada");

	}


	this.showPersonForm = false;
	}

	getPeopleData(e){

		//console.log("oteniendo datos")
		//console.log(e.detail.people)

		this.people = e.detail.people
		//this.requestUpdate();
	}
}
customElements.define('persona-main', PersonaMain)