import { LitElement, html } from 'lit-element';  
class PersonaForm2 extends LitElement {
    static get properties() {
        return {			
            person: {type: Object}
        };
    }
    
    constructor() {
        super();		
    
        this.person = {};		
    }
    
    render() {
        return html`	
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" 
	    integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" 
	    crossorigin="anonymous" />
            <div>
                <form>
                    <div class="form-group">
                        <label>Nombre Completo</label>
                        <input type="text" @input="${this.updateName}" id="personFormName" class="form-control" placeholder="Nombre Completo"/>
                    <div>
                    <div class="form-group">
                        <label>Género</label>
                        <textarea @input="${this.updateGender}" class="form-control" placeholder="Género" rows="5"></textarea>
                    <div>
                    <div class="form-group">
                        <label>Masa Corporal</label>
                        <input type="text" @input="${this.updateMass}" class="form-control" placeholder="Masa corporal"/>
                    <div>
                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                    <button @click="${this.storePerson}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>			
        `;
    }       	
    
    goBack(e) {
        console.log("goBack");	  
        e.preventDefault();	
        this.dispatchEvent(new CustomEvent("persona-form-close",{}));
    }
    
    updateName(e) {
        console.log("updateName");
        console.log("Actualizando la propiedad name con el valor " + e.target.value);
        this.person.name = e.target.value;
    }
    
    updateGender(e) {
        console.log("updateGender");
        console.log("Actualizando la propiedad Gender con el valor " + e.target.value);
        this.person.gender = e.target.value;
    }
    
    updateMass(e) {
        console.log("updateMass");
        console.log("Actualizando la propiedad mass con el valor " + e.target.value);
        this.person.mass = e.target.value;
    }
    
    storePerson(e) {
        console.log("storePerson");
        e.preventDefault();
        
        
            
        console.log("La propiedad name vale " + this.person.name);
        console.log("La propiedad gender vale " + this.person.gender);
        console.log("La propiedad mass vale " + this.person.mass);
            
        this.dispatchEvent(new CustomEvent("persona-form-store",{
            detail: {
                person:  {
                        name: this.person.name,
                        gender: this.person.gender,
                        mass: this.person.mass
                    }
                }
            })
        );
    }
}
customElements.define('persona-form2', PersonaForm2)