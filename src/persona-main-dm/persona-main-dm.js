import { LitElement, html } from 'lit-element';

class PersonaMainDm extends LitElement {

    static get properties() {
        return {
            people: {type: Array}
        };
    }

    constructor() {
        super();

        this.people = [ 
            {
                name: "Ellen Ripley",
                yearsInCompany: 10,
                profile: "Lorem ipsum dolor sit amet.",
                photo: {
                    "src": "./img/persona.jpg",
                    "alt": "Ellen Ripley"
                },
                canTeach: false				
            }, {
                name: "Bruce Banner",		
                yearsInCompany: 2,
                profile: "Lorem ipsum.",
                photo: {
                    "src": "./img/persona.jpg",
                    "alt": "Bruce Banner"
                },
                canTeach: true
            }, {
                name: "Éowyn",
                yearsInCompany: 5,
                profile: "Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
                photo: {
                    "src": "./img/persona.jpg",
                    "alt": "Éowyn"
                },
                canTeach: true
            }, {
                name: "Turanga Leela",
                yearsInCompany: 9,
                profile: "Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod.",
                photo: {
                    "src": "./img/persona.jpg",
                    "alt": "Turanga Leela"
                },
                canTeach: true
            }, {
                name: "Tyrion Lannister",
                yearsInCompany: 1,
                profile: "Lorem ipsum.",
                photo: {
                    "src": "./img/persona.jpg",
                    "alt": "Tyrion Lannister"
                },
                canTeach: false
            }
         ];
       
    }

    updated(changedProperties) {
        //console.log("updated");	
        if (changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en persona-main-dm");

            this.dispatchEvent(
                new CustomEvent("getPeopleData", {
                        detail: {
                            people: this.people
                        }
                    }
                    
                )
            )
            
        }
   
    }
}

customElements.define('persona-main-dm', PersonaMainDm)