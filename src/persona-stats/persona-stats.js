import { LitElement, html } from 'lit-element';  
class PersonaStats extends LitElement {
	static get properties() {
		return {
			people: {type: Array}			
		};
	}

	constructor() {
		super();
		this.people = [];			
	}

	updated(changedProperties) {
		//console.log ("update en persona stats");

		if (changedProperties.has("people")) {

			//console.log("ha cambiado el array people en persona stats");

			let peopleStats = this.gatherPeopleArrayInfo(this.people);

			//console.log("actualiza personas a " + peopleStats.numberOfPeople);

			this.dispatchEvent(
				new CustomEvent("updated-people-stats", {
						detail: {
							peopleStats: peopleStats
						}
					}
				)
			);
		}

	}

	gatherPeopleArrayInfo(people){

		

		let peopleStats = {};
		peopleStats.numberOfPeople = people.length;
		
		let maxYearsInCompany = 0;

		people.forEach(person => {
			if (person.yearsInCompany >= maxYearsInCompany){
				maxYearsInCompany = person.yearsInCompany;
			}
			
		});

		//console.log ("maxyearsincompany es " + maxYearsInCompany);
		peopleStats.maxYearsInCompany = maxYearsInCompany;
				
		return peopleStats;

	}

	    
    
}  
customElements.define('persona-stats', PersonaStats) 